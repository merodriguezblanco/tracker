var express  = require('express'),
    http     = require('http'),
    socketio = require('socket.io'),
    Swarm    = require('./modules/Swarm');
    Peer     = require('./modules/Peer');
    Stats    = require('./modules/Stats');

var serverPort = process.env.PORT || process.argv.pop();
serverPort = isNaN(serverPort) ? 6001 : serverPort;

var app = express();
var server = http.createServer(app);
var io = socketio.listen(server);

app.use(express.static(__dirname + '/public'));
app.set('view engine', 'ejs');

app.get('/stats', function (request, response) {
  response.render('../index', { data: Stats.getStats() });
});

app.get('/stats.json', function (request, response) {
  response.contentType('json');
  response.write(JSON.stringify(Stats.getStats()));
  response.end();
});

app.get('/reset_stats', function (request, response) {
  console.log('Resetting stats!');
  Stats.reset();
  response.send('Stats reset! \\o/');
});

server.listen(serverPort);
console.log('Tracker listening on port ' + serverPort);

io.sockets.on('connection', function (socket) {
  socket.on('initHostPeer', function () {
    socket.emit('peerInitialized', Swarm.createPeer());
  });

  socket.on('requestSwarm', function (data) {
    socket.emit('swarmReceived', Swarm.returnSwarm(data));
  });

  socket.on('keepAlive', function (data) {
    var peerId = data.peerId;
    Swarm.keepAlivePeer(data);
    Stats.process(peerId, data.remoteStats, data.videoStats);
  });

  socket.on('fileCompleted', function (data) {
    var peer = Peer.findActiveOrCreate(data.peerId);
    peer.setFileCompleted(true);
  });
});

var logStatus = function () {
  setInterval(function () {
    console.log('--------------- Swarm Status ---------------');
    console.log(Peer.active());
    console.log('------------------ Stats -------------------');
    console.log(Stats.getStats());
  }, 10000);
}

logStatus();

