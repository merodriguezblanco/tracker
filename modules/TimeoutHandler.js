var TimeoutHandler = function () {}

module.exports = TimeoutHandler;

var timeouts = {};
var counter = 0;

TimeoutHandler.setTimeout = function (callback, delay) {
  var current = counter;
  counter += 1;
  timeouts[current] = setTimeout(function() {
    timeouts[current] = null;
    callback();
  }, delay);
  return current;
}

TimeoutHandler.findTimeout = function (id) {
  return timeouts[id];
}

