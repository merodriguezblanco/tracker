var Stats = function () {}

module.exports = Stats;

var Peer  = require('./Peer'),
    CDN   = require('./Cdn'),
    Utils = require('./Utils'),
    _     = require('underscore');

Stats.totalChunksDelivered = 0;

var chunksDeliveredFromP2P = function () {
  return _.reduce(Peer.all(), function (memo, peer) {
    return memo + peer.getChunksDelivered();
  }, 0);
}

var chunksDeliveredFromCDNForCDN = function (cdnId) {
  return Utils.forResource(cdnId, CDN.className, function (cdn) {
    return cdn.getChunksDelivered();
  });
}

var chunksDeliveredFromCDN = function () {
  return _.reduce(CDN.all(), function (memo, cdn) {
    return memo + cdn.getChunksDelivered();
  }, 0);
}

var updateDeliveredChunksNumbers = function (newChunksDelivered) {
  Stats.totalChunksDelivered = Stats.totalChunksDelivered + newChunksDelivered;
}

Stats.reset = function () {
  Stats.totalChunksDelivered = 0;
  CDN.resetStats();
  Peer.resetStats();
}

Stats.process = function (peerId, remoteStats, videoStats) {
  Utils.forResource(peerId, Peer.className, function (peer) {
    updateDeliveredChunksNumbers(peer.processRemoteStats(remoteStats));
    peer.processVideoStats(videoStats);
  });
}

var getP2PPercentage = function () {
  return Utils.percentOf(chunksDeliveredFromP2P(), Stats.totalChunksDelivered);
}

var getCDNPercentage = function () {
  return Utils.percentOf(chunksDeliveredFromCDN(), Stats.totalChunksDelivered);
}

var getCDNPercentageForCDN = function (cdnId) {
  return Utils.percentOf(chunksDeliveredFromCDNForCDN(cdnId), Stats.totalChunksDelivered);
}

var getGlobalConsumptionStats = function () {
  return {
    cdn: getCDNPercentage(),
    cdn1: getCDNPercentageForCDN('CDN 1'),
    cdn2: getCDNPercentageForCDN('CDN 2'),
    p2p: getP2PPercentage()
  }
}

var getPeersStats = function () {
  return _.map(Peer.all(), function (peer) {
    return peer.getPeerStats();
  });
}

Stats.getStats = function () {
  return {
    globalConsumption: getGlobalConsumptionStats(),
    peersInfo: getPeersStats()
  }
}
