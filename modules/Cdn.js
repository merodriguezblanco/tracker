var CDN = function (name, url, weight) {
  this.id     = name;
  this.url    = url;
  this.name   = name;
  this.weight = weight;
  this.chunksDelivered = 0;

  var self = this;

  //
  //
  // STATS
  self.getChunksDelivered = function () {
    return self.chunksDelivered;
  }

  self.updateChunksDelivered = function (newChunksDelivered) {
    self.chunksDelivered += newChunksDelivered;
  }

  self.resetStats = function () {
    self.chunksDelivered = 0;
  }

}

module.exports = CDN;

var _ = require('underscore');

CDN.cdns = [
  new CDN('CDN 1', 'http://trial.goalbit-solutions.com/goalbit-misc/videos/', 1),
  new CDN('CDN 2', 'http://goalbit-solutions.com/www2011/images/videos/', 9)
];
CDN.className = 'cdn';

CDN.all = function () {
  return CDN.cdns;
}

CDN.find = function (cdnId) {
  return _.findWhere(CDN.all(), { id: cdnId });
}

CDN.resetStats = function () {
  _.each(CDN.all(), function (cdn) {
    cdn.resetStats();
  });
}
