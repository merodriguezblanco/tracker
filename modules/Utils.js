var Utils = function () {}

module.exports = Utils;

var Peer = require('./Peer'),
    CDN  = require('./Cdn'),
    _    = require('underscore');


Utils.getResource = function (resourceId, resourceType) {
  if (resourceType === CDN.className) {
    return CDN.find(resourceId);
  } else if (resourceType === Peer.className) {
    return Peer.find(resourceId);
  } else {
    console.log('ERROR:', ' Unsupported resourceType', resourceType);
  }
}

// Finds a resource and returns in the callback if exists
Utils.forResource = function (resourceId, resourceType, callback) {
  var resource = Utils.getResource(resourceId, resourceType);

  if (resource) {
    return callback(resource);
  } else {
    console.log('ERROR:', ' Resource not found for id', resourceId, 'and type', resourceType);
  }
}

Utils.percentOf = function (current, total) {
  return (total === 0) ? 0 : Math.round((current / total) * 100);
}

