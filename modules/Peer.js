var Peer = function (id) {
  this.id = id;
  this.isAlive = true;
  this.executionChunk = 0;
  this.fileCompleted = false;
  this.chunksDelivered = 0;
  this.chunksReceivedFromCDN = 0;
  this.chunksReceivedFromP2P = 0;
  this.totalWaitingTime = 0;
  this.startupWaitingTime = 0;
  this.waitingTimeAmount = 0;
  var timer = null;

  var self = this;

  self.kill = function () {
    console.log('Forgetting', self.id);
    self.isAlive = false;
  }

  self.installTimeout = function () {
    timer = TimeoutHandler.setTimeout(self.kill, self.TIMEOUT);
  }

  self.keepAlive = function (data) {
    clearTimeout(TimeoutHandler.findTimeout(timer));
    self.installTimeout();
    self.updateExecutionChunk(data.index);
    self.setFileCompleted(data.fileCompleted);
  }

  //
  //
  // STATS

  self.resetStats = function () {
    self.chunksDelivered = 0;
    self.chunksReceivedFromCDN = 0;
    self.chunksReceivedFromP2P = 0;
    self.totalWaitingTime = 0;
    self.startupWaitingTime = 0;
    self.waitingTimeAmount = 0;
  }

  self.getChunksDelivered = function () {
    return self.chunksDelivered;
  }

  self.updateChunksDelivered = function (newChunksDelivered) {
    self.chunksDelivered += newChunksDelivered;
  }

  var updateChunksReceived = function (newChunksReceived, seederType) {
    if (seederType === CDN.className) {
      self.chunksReceivedFromCDN += newChunksReceived;
    } else if (seederType === Peer.className) {
      self.chunksReceivedFromP2P += newChunksReceived;
    }
  }

  self.processRemoteStats = function (stats) {
    var numbers,
        amountDelivered = 0;

    eachStat(stats, function (seederId, seederInfo) {
      numbers = seederInfo.numbers;
      amountDelivered += numbers.chunksDelivered;

      updateChunksReceived(numbers.chunksDelivered, seederInfo.type);
      Utils.forResource(seederId, seederInfo.type, function (seeder) {
        seeder.updateChunksDelivered(numbers.chunksDelivered);
      });
    });

    return amountDelivered;
  }

  self.processVideoStats = function (stats) {
    self.totalWaitingTime   = stats.totalWaitingTime;
    self.startupWaitingTime = stats.startupWaitingTime;
    self.waitingTimeAmount  = stats.waitingTimeAmount;
  }

  // Iterates through the stats list returning the resourceId and the resource 
  // object for each element
  var eachStat = function (stats, callback) {
    var pairs = _.pairs(stats);
    _.each(pairs, function (pair) {
      callback(pair[0], pair[1]);
    });
  }

  self.getPeerStats = function () {
    var stats = {};
    stats[self.id] = {
      p2p: receivedChunksFromP2PPercentage(),
      cdn: receivedChunksFromCDNPercentage(),
      totalWaitingTime: self.totalWaitingTime,
      startupWaitingTime: self.startupWaitingTime,
      waitingTimeAmount: self.waitingTimeAmount
    }

    return stats;
  }

  var receivedChunksFromCDNPercentage = function () {
   return Utils.percentOf(self.chunksReceivedFromCDN, totalReceivedChunks());
  }

  var receivedChunksFromP2PPercentage = function () {
   return Utils.percentOf(self.chunksReceivedFromP2P, totalReceivedChunks());
  }

  var totalReceivedChunks = function () {
    return self.chunksReceivedFromP2P + self.chunksReceivedFromCDN;
  }

  self.updateExecutionChunk = function (executionChunk) {
    self.executionChunk = executionChunk;
  }

  self.setFileCompleted = function (fileCompleted) {
    self.fileCompleted = fileCompleted;
  }

  self.getSwarm = function () {
    var sortedLeechers = Peer.sortedLeechers();
    var peerLocation = _.indexOf(sortedLeechers, self);
    var nearestPeers = (peerLocation === -1) ? [] : calculateNearest(sortedLeechers, peerLocation);

    return fillUpWithSeeders(nearestPeers);
  }

  // Private Methods

  var calculateNearest = function(sortedPeers, peerIndex) {
    var afterIndex = peerIndex + 1,
      afterDistance,
      beforeIndex = peerIndex - 1,
      beforeDistance,
      outOfPeers = false,
      peerExecutionChunk = sortedPeers[peerIndex].executionChunk,
      result = [];

    if (sortedPeers.length > 1) {
      while ((result.length < MAX_NEAREST_PEERS) && !outOfPeers) {
        beforeDistance = getDistance(sortedPeers, peerExecutionChunk, beforeIndex);
        afterDistance = getDistance(sortedPeers, peerExecutionChunk, afterIndex);

        if (beforeDistance < afterDistance) {
          result.push(sortedPeers[beforeIndex]);
          beforeIndex--;
        } else if (afterDistance !== Number.MAX_VALUE) {
          result.push(sortedPeers[afterIndex]);
          afterIndex++;
        } else {
          outOfPeers = true;
        }
      }
    }

    return result;
  }

  var fillUpWithSeeders = function (nearestPeers) {
    return _.first(nearestPeers.concat(_.sample(Peer.seeder(), MAX_SEEDERS_RETURNED)), MAX_SWARM_SIZE);
  }

  var getDistance = function (sortedPeers, peerExecutionChunk, index) {
    if (index >= 0 && index < sortedPeers.length) {
      return Math.abs(peerExecutionChunk - sortedPeers[index].executionChunk);
    } else {
      return Number.MAX_VALUE;
    }
  }
}

module.exports = Peer;

var TimeoutHandler = require('./TimeoutHandler'),
    Utils          = require('./Utils'),
    CDN            = require('./Cdn'),
    _              = require('underscore');

var MAX_SWARM_SIZE = 10,
  MAX_NEAREST_PEERS = 8,
  MAX_SEEDERS_RETURNED = 3;

Peer.prototype.TIMEOUT = 9000 // milliseconds
Peer.className = 'peer';
Peer.peers = [];
Peer.idCounter = 0;

Peer.all = function () {
  return Peer.peers;
}

Peer.active = function () {
  return _.where(Peer.all(), { isAlive: true });
}

Peer.seeder = function () {
  return _.where(Peer.active(), { fileCompleted: true });
}

Peer.leechers = function () {
  return _.where(Peer.active(), { fileCompleted: false });
}

Peer.create = function () {
  var peer = new Peer(generatePeerId());
  Peer.peers.push(peer);
  peer.installTimeout();
  return peer;
}

Peer.sortedLeechers = function () {
  return _.sortBy(Peer.leechers(), function (iterPeer) { return iterPeer.executionChunk });
}

Peer.find = function (peerId) {
  return _.findWhere(Peer.all(), { id: peerId });
}

Peer.findActiveOrCreate = function (peerId) {
  var peer = Peer.findActive(peerId);
  if (peer) {
    return peer;
  } else {
    return reliveOrCreatePeeWithId(peerId);
  }
}

Peer.findActive = function (peerId) {
  return _.findWhere(Peer.active(), { id: peerId });
}

Peer.resetStats = function () {
  _.each(Peer.all(), function (peer) {
    peer.resetStats();
  });
}

var reliveOrCreatePeeWithId = function (peerId) {
  var peer = Peer.find(peerId);
  if (!peer) {
    peer = new Peer(peerId);
    resetIdGenerator(peerId);
    Peer.peers.push(peer);
    peer.installTimeout();
  } else {
    peer.isAlive = true;
    peer.installTimeout();
  }

  return peer;
}

var resetIdGenerator = function (peerId) {
  var idNumber = parseIdNumberFromGeneratedIdString(peerId);
  if (Peer.idCounter < idNumber) {
    Peer.idCounter = idNumber;
  }
}

var generatePeerId = function () {
  return 'Peer ' + incrementCounter();
}

var incrementCounter = function () {
  return ++Peer.idCounter;
}

var parseIdNumberFromGeneratedIdString = function (stringId) {
  return parseInt(stringId.split('Peer')[1]);
}

