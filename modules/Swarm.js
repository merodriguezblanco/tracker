var Swarm = function () {}

module.exports = Swarm;

var CDN  = require('./Cdn'),
    Peer = require('./Peer'),
    _    = require('underscore');

Swarm.returnSwarm = function (data) {
  var peer = Peer.findActiveOrCreate(data.peerId);
  peer.updateExecutionChunk(data.index);
  return {
    'peers': peer.getSwarm()
  }
}

Swarm.createPeer = function() {
  var peer = Peer.create();
  return {
    'cdns': CDN.all(),
    'host': peer
  }
}

Swarm.keepAlivePeer = function (data) {
  var peer = Peer.findActiveOrCreate(data.peerId);
  console.log(peer.id, 'sent keepAlive');
  peer.keepAlive(data);
}

